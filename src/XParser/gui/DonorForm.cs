﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XParser.io;
using XParser.net;

namespace XParser.gui
{
    public partial class DonorForm : Form
    {

        BindingList<Donor> list;

        public DonorForm()
        {
            InitializeComponent();
        }

        private void view_DoubleClick(object sender, EventArgs e)
        {
            if (view.SelectedItem == null) return;

            Donor donor = (Donor)view.SelectedItem;

            setFormContent(donor);
        }

        private void DonorForm_Load(object sender, EventArgs e)
        {
            try
            {
                list = new BindingList<Donor>(DonorIO.read());
            }
            catch (System.IO.FileNotFoundException)
            {
                list = new BindingList<Donor>();
            }

            view.DataSource = list;
            view.DisplayMember = "name";
            
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            bool emptyForm = false;
            if (this.Controls.OfType<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                emptyForm = true;
            }

            if (emptyForm)
            {
                MessageBox.Show("Не все поля заполнены", "Ошибка");
                return;
            }

            list[view.SelectedIndex] = new Donor
            {
                urlGeneral = textBoxUrl.Text,
                name = textBoxName.Text,
                listPath = getFormContent()
            };

            DonorIO.write(list.ToList());
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            list.RemoveAt(view.SelectedIndex);

            DonorIO.write(list.ToList());
        }

        private SortedDictionary <string, string> getFormContent()
        {
            foreach (TextBox t in this.Controls.OfType<TextBox>())
            {
                t.Clear();
            }

            SortedDictionary<string, string> list = new SortedDictionary<string, string>();

            list.Add("name", @textBoxXName.Text);
            list.Add("weight", @textBoxXWeight.Text);
            list.Add("price_old", @textBoxXPriceOld.Text);
            list.Add("price_new", @textBoxXPriceNew.Text);
            list.Add("price_stand", @textBoxXPriceStand.Text);
            list.Add("dicsount", @textBoxXDiscount.Text);
            list.Add("avi", @textBoxXAvi.Text);

            list.Add("des-1", textBoxXDes1.Text);
            list.Add("des-2", textBoxXDes2.Text);
            list.Add("des-3", textBoxXDes3.Text);

            return list;
        }

        private void setFormContent(Donor donor)
        {
            textBoxName.Text = donor.name;
            textBoxUrl.Text = donor.urlGeneral;

            textBoxXName.Text = @donor.listPath["name"];
            textBoxXWeight.Text = @donor.listPath["weight"];
            textBoxXPriceOld.Text = @donor.listPath["price_old"];
            textBoxXPriceNew.Text = @donor.listPath["price_new"];
            textBoxXPriceStand.Text = @donor.listPath["price_stand"];
            textBoxXDiscount.Text = @donor.listPath["dicsount"];
            textBoxXAvi.Text = @donor.listPath["avi"];

            textBoxXDes1.Text = @donor.listPath["des-1"];
            textBoxXDes2.Text = @donor.listPath["des-2"];
            textBoxXDes3.Text = @donor.listPath["des-3"];

            groupBox1.Visible = true;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            list.Add(new Donor
            {
                urlGeneral = "http://new.url.com",
                name = "new.url.com",
                listPath = getFormContent()
            });

            Console.WriteLine("list size: {0}", list.Count);

            view.SelectedIndex = list.Count - 1;
            view.SelectedItem = list.Last();
            setFormContent(list.Last());

            DonorIO.write(list.ToList());
        }
    }
}
