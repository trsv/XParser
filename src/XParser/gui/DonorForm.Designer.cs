﻿namespace XParser.gui
{
    partial class DonorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.view = new System.Windows.Forms.ListBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxXAvi = new System.Windows.Forms.TextBox();
            this.textBoxXDiscount = new System.Windows.Forms.TextBox();
            this.textBoxXPriceStand = new System.Windows.Forms.TextBox();
            this.textBoxXPriceNew = new System.Windows.Forms.TextBox();
            this.textBoxXPriceOld = new System.Windows.Forms.TextBox();
            this.textBoxXWeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxXName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonNew = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxXDes1 = new System.Windows.Forms.TextBox();
            this.textBoxXDes2 = new System.Windows.Forms.TextBox();
            this.textBoxXDes3 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // view
            // 
            this.view.FormattingEnabled = true;
            this.view.ItemHeight = 16;
            this.view.Location = new System.Drawing.Point(12, 12);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(190, 468);
            this.view.TabIndex = 0;
            this.view.DoubleClick += new System.EventHandler(this.view_DoubleClick);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(6, 401);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(100, 28);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.buttonRemove);
            this.groupBox1.Controls.Add(this.buttonSave);
            this.groupBox1.Controls.Add(this.textBoxUrl);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(208, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 435);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Донор";
            this.groupBox1.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxXDes3);
            this.groupBox2.Controls.Add(this.textBoxXDes2);
            this.groupBox2.Controls.Add(this.textBoxXDes1);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textBoxXAvi);
            this.groupBox2.Controls.Add(this.textBoxXDiscount);
            this.groupBox2.Controls.Add(this.textBoxXPriceStand);
            this.groupBox2.Controls.Add(this.textBoxXPriceNew);
            this.groupBox2.Controls.Add(this.textBoxXPriceOld);
            this.groupBox2.Controls.Add(this.textBoxXWeight);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBoxXName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(9, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(344, 307);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "XPath";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Наличие";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Dicsount";
            // 
            // textBoxXAvi
            // 
            this.textBoxXAvi.Location = new System.Drawing.Point(104, 183);
            this.textBoxXAvi.Name = "textBoxXAvi";
            this.textBoxXAvi.Size = new System.Drawing.Size(234, 22);
            this.textBoxXAvi.TabIndex = 16;
            // 
            // textBoxXDiscount
            // 
            this.textBoxXDiscount.Location = new System.Drawing.Point(104, 155);
            this.textBoxXDiscount.Name = "textBoxXDiscount";
            this.textBoxXDiscount.Size = new System.Drawing.Size(234, 22);
            this.textBoxXDiscount.TabIndex = 15;
            // 
            // textBoxXPriceStand
            // 
            this.textBoxXPriceStand.Location = new System.Drawing.Point(104, 127);
            this.textBoxXPriceStand.Name = "textBoxXPriceStand";
            this.textBoxXPriceStand.Size = new System.Drawing.Size(234, 22);
            this.textBoxXPriceStand.TabIndex = 14;
            // 
            // textBoxXPriceNew
            // 
            this.textBoxXPriceNew.Location = new System.Drawing.Point(104, 99);
            this.textBoxXPriceNew.Name = "textBoxXPriceNew";
            this.textBoxXPriceNew.Size = new System.Drawing.Size(234, 22);
            this.textBoxXPriceNew.TabIndex = 13;
            // 
            // textBoxXPriceOld
            // 
            this.textBoxXPriceOld.Location = new System.Drawing.Point(104, 71);
            this.textBoxXPriceOld.Name = "textBoxXPriceOld";
            this.textBoxXPriceOld.Size = new System.Drawing.Size(234, 22);
            this.textBoxXPriceOld.TabIndex = 12;
            // 
            // textBoxXWeight
            // 
            this.textBoxXWeight.Location = new System.Drawing.Point(104, 43);
            this.textBoxXWeight.Name = "textBoxXWeight";
            this.textBoxXWeight.Size = new System.Drawing.Size(234, 22);
            this.textBoxXWeight.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "Цена (stand)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Цена (new)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Цена (old)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Вес";
            // 
            // textBoxXName
            // 
            this.textBoxXName.Location = new System.Drawing.Point(104, 15);
            this.textBoxXName.Name = "textBoxXName";
            this.textBoxXName.Size = new System.Drawing.Size(234, 22);
            this.textBoxXName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Название";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(112, 401);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(104, 28);
            this.buttonRemove.TabIndex = 6;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(113, 43);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(234, 22);
            this.textBoxUrl.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ссылка";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(113, 15);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(234, 22);
            this.textBoxName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название";
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(467, 453);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(100, 28);
            this.buttonNew.TabIndex = 3;
            this.buttonNew.Text = "Новый";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 214);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Описание #1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 17);
            this.label11.TabIndex = 20;
            this.label11.Text = "Описание #2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 270);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 17);
            this.label12.TabIndex = 21;
            this.label12.Text = "Описание #3";
            // 
            // textBoxXDes1
            // 
            this.textBoxXDes1.Location = new System.Drawing.Point(104, 211);
            this.textBoxXDes1.Name = "textBoxXDes1";
            this.textBoxXDes1.Size = new System.Drawing.Size(234, 22);
            this.textBoxXDes1.TabIndex = 22;
            // 
            // textBoxXDes2
            // 
            this.textBoxXDes2.Location = new System.Drawing.Point(104, 239);
            this.textBoxXDes2.Name = "textBoxXDes2";
            this.textBoxXDes2.Size = new System.Drawing.Size(234, 22);
            this.textBoxXDes2.TabIndex = 23;
            // 
            // textBoxXDes3
            // 
            this.textBoxXDes3.Location = new System.Drawing.Point(104, 267);
            this.textBoxXDes3.Name = "textBoxXDes3";
            this.textBoxXDes3.Size = new System.Drawing.Size(234, 22);
            this.textBoxXDes3.TabIndex = 24;
            // 
            // DonorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 493);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.view);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DonorForm";
            this.Text = "Доноры";
            this.Load += new System.EventHandler(this.DonorForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox view;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxXPriceStand;
        private System.Windows.Forms.TextBox textBoxXPriceNew;
        private System.Windows.Forms.TextBox textBoxXPriceOld;
        private System.Windows.Forms.TextBox textBoxXWeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxXName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxXAvi;
        private System.Windows.Forms.TextBox textBoxXDiscount;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.TextBox textBoxXDes3;
        private System.Windows.Forms.TextBox textBoxXDes2;
        private System.Windows.Forms.TextBox textBoxXDes1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}