﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using XParser.net;
using XParser.gui;

namespace XParser
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void buttonStart_Click(object sender, EventArgs e)
        {
            string @path = textBoxPath.Text;

            try
            {
                DonorHandler handler = new DonorHandler(path, Convert.ToInt32(numericColumn.Value), checkHeader.Checked);

                var progress = new Progress<DonorHandler>();
                progress.ProgressChanged += (o, task) =>
                {
                    progressBar.Value = task.progressPercent;
                    progressBar.Update();

                    labelProgress.Text = task.progressMessage;
                };

                buttonStart.Enabled = false;
                progressBar.Visible = true;
                labelProgress.Visible = true;

                await handler.process(progress);

                buttonStart.Enabled = true;
            }
            catch (Exception ex)
            {
                buttonStart.Enabled = true;
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "MS Excel Files|*.xlsx";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxPath.Text = dialog.FileName;
                labelProgress.Text = "Файл с артикулами добавлен";
            }
        }

        private void донорыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DonorForm donorForm = new DonorForm();
            donorForm.StartPosition = FormStartPosition.CenterParent;
            donorForm.ShowDialog(this);
        }
    }
}
