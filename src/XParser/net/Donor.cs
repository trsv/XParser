﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

using HtmlAgilityPack;
using XParser.util;
using XParser.io;
using System.Text.RegularExpressions;

namespace XParser.net
{
    class Donor
    {
        public string name { get; set; }
        public string urlGeneral { get; set; }
        public SortedDictionary<string, string> listPath { get; set; }

        public async Task <Product> parseByArticle(string article)
        {
            Product p = await searchTheArticle(article);
            return p;
            //ArticleIO.WriteXslx(p);
        }

        private async Task<Product> searchTheArticle(string art)
        {
            Product product = new Product();

            using (HttpClient client = new HttpClient())
            {
                Uri url = new Uri("https://www.swansonvitamins.com/q?kw=" + art);

                Console.WriteLine("Go to: {0}", url.ToString());

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Host", url.Host);
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                using (var response = await client.GetAsync(url))
                {
                    using (var content = response.Content)
                    {
                        string mycontent = await content.ReadAsStringAsync(); 

                        foreach (KeyValuePair<string, string> entry in listPath)
                        {
                            if (string.IsNullOrWhiteSpace(entry.Value)) continue;

                            string value = parseByPath(ref mycontent, entry.Value);

                            product.setValueByName(entry.Key, value);
                        }

                        product.article = art;
                        product.url = response.RequestMessage.RequestUri.ToString();
                    }
                }

            }

            return product;
        }

        private string parseByPath(ref string html, string xpath)
        {
            html = HtmlUtils.closeTags(html);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            try
            {

                if (doc.DocumentNode == null) throw new ArgumentException();

                HtmlNode node = doc.DocumentNode.SelectSingleNode(xpath);

                if (node != null)
                {
                    string value = HtmlEntity.DeEntitize(node.InnerText.Trim());
                    value = Regex.Replace(value, @"\s+", " ");
                    return value;
                }
                else return "";
            }
            catch (ArgumentException e)
            {
                // эксепшены с другого потока не ловятся в Form1
                throw new ArgumentException("Не удалось парсить контент", e);
            }
        }
    }
}