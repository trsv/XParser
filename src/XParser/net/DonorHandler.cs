﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XParser.io;

namespace XParser.net
{
    class DonorHandler
    {
        public int progressPercent { get; set; }
        public string progressMessage { get; set; }

        private List<Donor> listDonor;
        private List<string> listArtile;


        public DonorHandler(string @pathToArticles, int column, bool hasHeader)
        {
            listDonor = DonorIO.read(true);
            listArtile = ArticleIO.read(pathToArticles, column, hasHeader);
        }

        public Task process(IProgress<DonorHandler> progress)
        {
            return Task.Run(() =>
            {
                string @path = "result-" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".xlsx";

                foreach (Donor donor in listDonor)
                {
                    this.progressMessage = "Парсим: " + donor.name;

                    int count = 0;
                    List<Product> listProduct = new List<Product>();
                    foreach (string article in listArtile)
                    {
                        Product product = donor.parseByArticle(article).Result;
                        listProduct.Add(product);
                        this.progressPercent = ++count * 100 / listArtile.Count;

                        progress.Report(this);
                    }

                    listProduct.ForEach(p => p.print());
                    ArticleIO.WriteXslx(listProduct, path);

                    

                    progress.Report(this);
                }

                this.progressPercent = 100;
                this.progressMessage = "Готово";

                progress.Report(this);

            });
        }

    }
}
