﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace XParser.io
{
    class Product
    {
        public string name { get; set; } // наименование
        public string article { get; set; } // артикл
        public string weight { get; set; } // вес
        public string old_price { get; set; } // старая цена
        public string new_price { get; set; } // новая цена
        public string price_stad { get; set; } // цена stad
        public string discount { get; set; } // скидка
        public string avi { get; set; } // наличие
        public string url { get; set; } // ссылка

        public string des1 { get; set; }
        public string des2 { get; set; }
        public string des3 { get; set; }

        public void setValueByName(string name, string value)
        {
            if (name.Equals("avi")) avi = value;
            if (name.Equals("dicsount")) discount = value;
            if (name.Equals("name")) this.name = value;
            if (name.Equals("price_new")) new_price = value;
            if (name.Equals("price_old")) old_price = value;
            if (name.Equals("price_stand")) price_stad = value;
            if (name.Equals("weight")) weight = value;

            if (name.Equals("des-1")) des1 = value;
            if (name.Equals("des-2")) des2 = value;
            if (name.Equals("des-3")) des3 = value;
        }

        public void print()
        {
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this))
            {
                string nameVar = descriptor.Name;
                object value = descriptor.GetValue(this);
                Console.WriteLine("{0} = {1}", nameVar, value);
            }
            Console.WriteLine("--------------");
        }
    }
}
