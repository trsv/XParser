﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OfficeOpenXml;

namespace XParser.io
{
    class ArticleIO
    {
        public static List<string> read(string @path, int column, bool hasHeader = false)
        {

            try
            {
                List<string> list = new List<string>();

                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    ExcelPackage excel = new ExcelPackage(fileStream);
                    var sheet = excel.Workbook.Worksheets.First();
                    var startRow = hasHeader ? 2 : 1;

                    for (int row = startRow; row <= sheet.Dimension.End.Row; row++)
                    {
                        try
                        {
                            string value = sheet.Cells[row, column].Value.ToString().Trim();

                            list.Add(value);
                        }
                        catch (NullReferenceException)
                        {
                            break;
                        }

                    }
                }

                return list;
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Файл с артикулами не найден", e);
            }
        }

        public static void WriteXslx(List<Product> listInfo, string @path)
        {
            FileInfo newFile = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(newFile);

            if (excel.Workbook.Worksheets.Count == 0) excel.Workbook.Worksheets.Add("Content");
            var sheet = excel.Workbook.Worksheets.First();

            int row = 0;

            if (!newFile.Exists)
            {
                sheet.Cells[1, 1].Value = "Наименование";
                sheet.Cells[1, 2].Value = "Артикул";
                sheet.Cells[1, 3].Value = "Вес";
                sheet.Cells[1, 4].Value = "Старая цена";
                sheet.Cells[1, 5].Value = "Новая цена";
                sheet.Cells[1, 6].Value = "Цена stand";
                sheet.Cells[1, 7].Value = "Скидка";
                sheet.Cells[1, 8].Value = "Наличие";
                sheet.Cells[1, 9].Value = "Ссылка";

                sheet.Cells[1, 10].Value = "Описание 1";
                sheet.Cells[1, 11].Value = "Описание 2";
                sheet.Cells[1, 12].Value = "Описание 3";

                row = 2;
            }
            else
            {
                row = sheet.Dimension.End.Row + 1;
            }

            for (int i = 0; i < listInfo.Count; row++, i++)
            {
                try
                {
                    sheet.Cells[row, 1].Value = listInfo[i].name;
                    sheet.Cells[row, 2].Value = listInfo[i].article;
                    sheet.Cells[row, 3].Value = listInfo[i].weight;
                    sheet.Cells[row, 4].Value = listInfo[i].old_price;
                    sheet.Cells[row, 5].Value = listInfo[i].new_price;
                    sheet.Cells[row, 6].Value = listInfo[i].price_stad;
                    sheet.Cells[row, 7].Value = listInfo[i].discount;
                    sheet.Cells[row, 8].Value = listInfo[i].avi;
                    sheet.Cells[row, 9].Value = listInfo[i].url;

                    sheet.Cells[row, 10].Value = listInfo[i].des1;
                    sheet.Cells[row, 11].Value = listInfo[i].des2;
                    sheet.Cells[row, 12].Value = listInfo[i].des3;
                }
                catch (NullReferenceException)
                {
                    break;
                }

            }

            excel.Save();
        }

        public static void WriteXslx(Product product, string @path)
        {
            FileInfo newFile = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(newFile);

            if (excel.Workbook.Worksheets.Count == 0) excel.Workbook.Worksheets.Add("Content");

            var sheet = excel.Workbook.Worksheets.First();

            int row = 0;

            if (!newFile.Exists)
            {
                sheet.Cells[1, 1].Value = "Наименование";
                sheet.Cells[1, 2].Value = "Артикул";
                sheet.Cells[1, 3].Value = "Вес";
                sheet.Cells[1, 4].Value = "Старая цена";
                sheet.Cells[1, 5].Value = "Новая цена";
                sheet.Cells[1, 6].Value = "Цена stand";
                sheet.Cells[1, 7].Value = "Скидка";
                sheet.Cells[1, 8].Value = "Наличие";
                sheet.Cells[1, 9].Value = "Ссылка";

                sheet.Cells[1, 10].Value = "Описание 1";
                sheet.Cells[1, 11].Value = "Описание 2";
                sheet.Cells[1, 12].Value = "Описание 3";

                row = 2;
            }
            else
            {
                row = sheet.Dimension.End.Row + 1;
            }

            try
            {
                sheet.Cells[row, 1].Value = product.name;
                sheet.Cells[row, 2].Value = product.article;
                sheet.Cells[row, 3].Value = product.weight;
                sheet.Cells[row, 4].Value = product.old_price;
                sheet.Cells[row, 5].Value = product.new_price;
                sheet.Cells[row, 6].Value = product.price_stad;
                sheet.Cells[row, 7].Value = product.discount;
                sheet.Cells[row, 8].Value = product.avi;
                sheet.Cells[row, 9].Value = product.url;

                sheet.Cells[row, 10].Value = product.des1;
                sheet.Cells[row, 11].Value = product.des2;
                sheet.Cells[row, 12].Value = product.des3;
            }
            catch (NullReferenceException)
            {

            }


            excel.Save();
        }

    }
}
