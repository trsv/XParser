﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XParser.net;
using Newtonsoft.Json;
using System.IO;

namespace XParser.io
{
    class DonorIO
    {

        public const string PATH_TO_DONORS = "api/donors.json";

        static DonorIO()
        {
            Directory.CreateDirectory("api");
        }

        public static List <Donor> read(bool fromMain = false)
        {
            try
            {
                string value = File.ReadAllText(PATH_TO_DONORS);
                List<Donor> list = JsonConvert.DeserializeObject<List<Donor>>(value);

                if (list.Count == 0 && fromMain) throw new ArgumentException();

                return list;
            }
            catch (DirectoryNotFoundException e)
            {
                throw new DirectoryNotFoundException("Файл с донорами не найден", e);
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException("Файл с донорами пуст", e);
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Доноры не найдены.\nСоздайте их с помощью меню 'Доноры'", e);
            }
        }

        public static void write(List <Donor> list)
        {
            string value = JsonConvert.SerializeObject(list);

            File.WriteAllText(PATH_TO_DONORS, value);
        }
    }
}